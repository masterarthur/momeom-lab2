[x, y] = meshgrid(-1:0.1:1, 0:0.1:1);
z = 4*sin(2*pi.*x).*cos(1.5*pi*exp(1)).*(1-x.^2).*y.*(1-y);

figure(1);
plot3(x, y, z);

figure(2);
subplot(2,2,1);
surf(x, y, z);

subplot(2,2,2);
mesh(x, y, z);

figure(3);
subplot(2,2,1);
contour3(x, y, z);

subplot(2,2,2);
contour3(x, y, z, 50);

figure(4);
subplot(2,2,1);
[C,h] = contour(x,y,z);   
clabel(C,h)

subplot(2,2,2);
[C,h] = contourf(x,y,z);   
clabel(C,h)