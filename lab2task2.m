clc
a = -2*pi;
b = 2*pi;
h = 2*pi/20;

x = a:h:b;
y = abs(tan(x)).*0.1;
z = (1+x).^6;

disp("x:");
disp(x);
disp("y:");
disp(y);
disp("z:");
disp(z);

figure(1);
plot(x,y,x,z);
grid on;
set(gca, 'Xscale', 'log');

figure(2);
plot(x,y,x,z);
grid on;
set(gca, 'Yscale', 'log');


figure(3);
plot(x,y,x,z);
grid on;
set(gca, 'Xscale', 'log');
set(gca, 'Yscale', 'log');