N=3;
C=0;
M=3; 

A=[1 3 N; 2 C M; N 9 8];
B=[6 C N; M C 1; 3 5 2];
C=[C M N; C C N; M N C];

At=A';
Bt=B';

K=[A B; At Bt];
figure(1);
spy(K);
figure(2);
imagesc(K);
figure(3);
surf(K);