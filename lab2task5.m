[X, Y] = meshgrid(-2*pi:1:2*pi);
Z = sin(2.*X).*cos(Y);

figure(1);
meshc(X,Y,Z);

figure(2);
contour(X,Y,Z);

figure(3);
surf(X,Y,Z);